# Security Releases general information

Security vulnerabilities in GitLab and its dependencies are to be addressed with
the highest priority.

Security releases are naturally very similar to [patch releases](../patch/process.md), but
on a much shorter timeline. The goal is to make a security release available as
soon as possible, while ensuring that the security issue is properly addressed
and that the fix does not introduce regressions.

At GitLab, we have two types of security releases:

 * [critical](#critical-security-release): immediate patch and mitigation is required for a single issue
 * [non-critical](#non-critical-security-releases): monthly release of planned issue

Also see [the GitLab Maintenance Policy](https://docs.gitlab.com/ee/policy/maintenance.html)
for the information on determining supported releases and assigning versions.

# General process overview

Release manager process is [described here](release-manager.md). The release
manager also makes sure that all the deadlines (described below) are
respected. Any delay needs to be escalated to Director of Backend and
Security.

Security engineers need to follow the process [defined here](security-engineer.md).

Developers need to follow the the process [defined here](developer.md).

Quality engineers need to follow the process [defined here](quality.md).

# Non-critical Security Releases

Every 28th of the month, we are releasing patch versions of GitLab containing
non-critical security fixes. This date has been chosen as it gives enough
time to release regular patch releases after standard release on the 22nd of
the month. This date also allows us to create and test fixes internally as
the next development cycle (starting on the 8th) starts.

## Release deadlines

Anything that can be carried out before the deadline, should be done.
Anything that is not done by the deadline requires immediate escalation to
people responsible of security release process.
Dates in this section can be considered as non-negotiable deadlines.

Non-critical security release contains security fixes that are ready for merge
**by 23:59 Pacific time on the 23rd** of the month. Both the fixes for the
current release and *all* of the backports need to be ready for merge and have
green tests.

If fixes and backports are not ready by that date, they will
have to go into the next security release.
**Exceptions can't be made** because verifying and creating security releases
are time consuming and any delay puts regular releases in danger.

Current release and all backports need to be tagged and packages created
**by 23:59 PT on the 24th**. This is also the deadline for creating the initial
blog post.

Once the packages are ready and up until **23:59 Pacific time on the 26th** of the
month, Quality Assurance tasks need to be carried out on *all* created releases to
verify that the fixes are indeed working as intended. During this period,
the tests should be executed on separate infrastructure created to carry out these tests.
Staging/canary/production are should remain open for regular releases.

Deploy to staging and canary environments should be carried out by **23:59 Pacific time
on the 27th** the latest.
Deploy to production has to be completed by **07:00 Pacific time on the 28th**.
Package promotion has to be coordinated with the blog post release. 1 hour before
the scheduled blog post announcement, package promotion should be done. This is
required because package promotion takes time to propagate.

Blog post, tweet and the email announcement should be complete by **10:00 Pacific time on the 28th**.

# Critical Security Releases

Depending on the severity and the attack surface of the vulnerability, an
immediate patch release consisting of just the security fix may be warranted.

The critical release process is a superset of a non-critical release, and for
completeness the process is written here:

1. The vulnerability has been reported and discussed between a [Security
   Engineer] and a [Developer]. When the timeline of a possible fix is
   established, the Security Engineer informs a [Release Manager] of a need
   for the critical security release.
1. A release manager proposes a timeline for the critical security release based
   on the other release tasks, and informs a security engineer and a [Quality
   Engineer] in the designated release issue.
1. Depending on the nature of the issue, a security engineer might need to
   create a temporary mitigation strategy together with a developer to be applied to
   possibly-affected environments. This can be one or more production
   patches, in cooperation with a production engineer.
1. A security engineer is assigned to determine the affected users and any
   further required investigation.
1. A security engineer works on a blog post to pre-announce the critical
   security release. They also work with the marketing team to inform the
   customers of an upcoming release. The security engineer also prepares the
   second blog post which will be published at release time.
1. Once a fix is ready, the developer assigns merge requests to the release
   managers.
1. A release manager prepares the release with all backports, and deploys to
   staging (if applicable).
1. A quality engineer prepares environments and executes QA tasks together with
   the security engineer to verify that all released versions have successfully
   resolved the vulnerability.
1. When the QA is completed, release manager deploys to the other environments.
1. When all environments contain the fix, any temporary mitigation strategy is
   reverted.
1. Following deployment, a release manager coordinates with a security engineer
   on the exact timing of a release.
1. A release manager will promote the packages at the designated time, and merge
   the release blog post.
1. A security engineer works with the marketing team to send notification emails
   to any affected users.
1. A release manager closes all release issues.
1. A security engineer keeps the issues where the vulnerabilities were reported
   open for the next 30 days.
1. A security engineer prepares a blog post that explains the vulnerability in
   detail and releases it approximately 30 days after the original release.
1. Once the final blog post is released, a security engineer removes the
   confidentiality from the issues and closes them.

Each involved role should follow their own guide, and create separate issues
linking to the main release issue.

# Guides by role

- [Release Manager]
- [Security Engineer]
- [Developer]
- [Quality Engineer]

---

[Return to Guides](../README.md)

[Release Manager]: release-manager.md
[Security Engineer]: security-engineer.md
[Developer]: developer.md
[Quality Engineer]: quality.md
